<?php
namespace AdminModules\Custom\NovumDigid\Systeem\Tables\Base;

use AdminModules\GenericEditController;
use Crud\Custom\NovumDigid\Tables\CrudTablesManager;
use Crud\FormManager;

/**
 * This class is automatically generated, do not modify manually.
 * Modify AdminModules\Custom\NovumDigid\Systeem\Tables instead if you need to override or add functionality.
 */
abstract class EditController extends GenericEditController
{
	public function getCrudManager(): FormManager
	{
		return new CrudTablesManager();
	}


	public function getPageTitle(): string
	{
		return "Data modellen";
	}
}
