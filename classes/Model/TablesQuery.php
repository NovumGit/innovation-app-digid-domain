<?php

namespace Model\Custom\NovumDigid;

use Model\Custom\NovumDigid\Base\TablesQuery as BaseTablesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tables' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TablesQuery extends BaseTablesQuery
{

}
