<?php
namespace Crud\Custom\NovumDigid;

trait CrudTrait
{
	public function getTags()
	{
		return ["NovumDigid"];
	}
}
