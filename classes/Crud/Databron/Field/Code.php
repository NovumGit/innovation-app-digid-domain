<?php
namespace Crud\Custom\NovumDigid\Databron\Field;

use Crud\Custom\NovumDigid\Databron\Field\Base\Code as BaseCode;

/**
 * Skeleton subclass for representing code field from the databron table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Code extends BaseCode
{
}
