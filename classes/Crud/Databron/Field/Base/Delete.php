<?php 
namespace Crud\Custom\NovumDigid\Databron\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumDigid\Databron;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Databron)
		{
		     return "/custom/novumdigid/systeem/databron/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Databron)
		{
		     return "/custom/novumdigid/databron?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
