<?php
namespace Crud\Custom\NovumDigid\Modules\Base;

use Crud\Custom\NovumDigid;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumDigid\Map\ModulesTableMap;
use Model\Custom\NovumDigid\Modules;
use Model\Custom\NovumDigid\ModulesQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Modules instead if you need to override or add functionality.
 */
abstract class CrudModulesManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumDigid\CrudTrait;
	use NovumDigid\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return ModulesQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumDigid\Map\ModulesTableMap();
	}


	public function getShortDescription(): string
	{
		return "Modules bepalen hoe het systeem er op het hoogste niveau uitziet.";
	}


	public function getEntityTitle(): string
	{
		return "Modules";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumdigid/systeem/modules/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumdigid/systeem/modules/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Modules toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Modules aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Title', 'Icon', 'Name', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Title', 'Icon', 'Name'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Modules
	 */
	public function getModel(array $aData = null): Modules
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oModulesQuery = ModulesQuery::create();
		     $oModules = $oModulesQuery->findOneById($aData['id']);
		     if (!$oModules instanceof Modules) {
		         throw new LogicException("Modules should be an instance of Modules but got something else." . __METHOD__);
		     }
		     $oModules = $this->fillVo($aData, $oModules);
		} else {
		     $oModules = new Modules();
		     if (!empty($aData)) {
		         $oModules = $this->fillVo($aData, $oModules);
		     }
		}
		return $oModules;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Modules
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Modules
	{
		$oModules = $this->getModel($aData);


		 if(!empty($oModules))
		 {
		     $oModules = $this->fillVo($aData, $oModules);
		     $oModules->save();
		 }
		return $oModules;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Modules $oModel
	 * @return Modules
	 */
	protected function fillVo(array $aData, Modules $oModel): Modules
	{
		isset($aData['title']) ? $oModel->setTitle($aData['title']) : null;
		isset($aData['icon']) ? $oModel->setIcon($aData['icon']) : null;
		isset($aData['name']) ? $oModel->setName($aData['name']) : null;
		return $oModel;
	}
}
