<?php
namespace Crud\Custom\NovumDigid\Modules\Field;

use Crud\Custom\NovumDigid\Modules\Field\Base\Icon as BaseIcon;

/**
 * Skeleton subclass for representing icon field from the modules table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Icon extends BaseIcon
{
}
