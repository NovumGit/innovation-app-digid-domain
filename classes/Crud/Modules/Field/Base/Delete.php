<?php 
namespace Crud\Custom\NovumDigid\Modules\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumDigid\Modules;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Modules)
		{
		     return "/custom/novumdigid/systeem/modules/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Modules)
		{
		     return "/custom/novumdigid/modules?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
