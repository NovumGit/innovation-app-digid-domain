<?php
namespace Crud\Custom\NovumDigid\Modules;

/**
 * Skeleton subclass for representing a Modules.
 *
 *
 *
 *
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class CrudModulesManager extends Base\CrudModulesManager
{
}
