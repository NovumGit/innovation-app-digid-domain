<?php 
namespace Crud\Custom\NovumDigid\Formulier\Field\Base;

use Crud\Generic\Field\GenericCheckbox;
use Crud\IEventField;
use Crud\IField;

class Checkbox extends GenericCheckbox implements IField, IEventField
{
}
