<?php
namespace Crud\Custom\NovumDigid\Tables\Base;

use Crud\Custom\NovumDigid;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IConfigurableCrud;
use Exception\LogicException;
use Model\Custom\NovumDigid\Map\TablesTableMap;
use Model\Custom\NovumDigid\Tables;
use Model\Custom\NovumDigid\TablesQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Map\TableMap;

/**
 * This class is automatically generated, do not modify manually.
 * Modify Tables instead if you need to override or add functionality.
 */
abstract class CrudTablesManager extends FormManager implements IConfigurableCrud, IApiExposable
{
	use NovumDigid\CrudTrait;
	use NovumDigid\CrudApiTrait;

	public function getQueryObject(): ModelCriteria
	{
		return TablesQuery::create();
	}


	public function getTableMap(): TableMap
	{
		return new \Model\Custom\NovumDigid\Map\TablesTableMap();
	}


	public function getShortDescription(): string
	{
		return "Modellen bepalen welke soorten data er in het systeem voorkomen.";
	}


	public function getEntityTitle(): string
	{
		return "Tables";
	}


	public function getOverviewUrl(): string
	{
		return "/custom/novumdigid/systeem/tables/overview";
	}


	public function getEditUrl(): string
	{
		return "/custom/novumdigid/systeem/tables/edit";
	}


	public function getCreateNewUrl(): string
	{
		return $this->getEditUrl();
	}


	public function getNewFormTitle(): string
	{
		return "Data modellen toevoegen";
	}


	public function getEditFormTitle(): string
	{
		return "Data modellen aanpassen";
	}


	public function getDefaultOverviewFields(): array
	{
		return ['Name', 'Icon', 'Title', 'IForm', 'Type', 'Required', 'Delete', 'Edit'];
	}


	public function getDefaultEditFields(): array
	{
		return ['Name', 'Icon', 'Title', 'IForm', 'Type', 'Required'];
	}


	/**
	 * Returns a model object of the type that this CrudManager represents.
	 * @param array $aData
	 * @return Tables
	 */
	public function getModel(array $aData = null): Tables
	{
		if (isset($aData['id']) && $aData['id']) {
		     $oTablesQuery = TablesQuery::create();
		     $oTables = $oTablesQuery->findOneById($aData['id']);
		     if (!$oTables instanceof Tables) {
		         throw new LogicException("Tables should be an instance of Tables but got something else." . __METHOD__);
		     }
		     $oTables = $this->fillVo($aData, $oTables);
		} else {
		     $oTables = new Tables();
		     if (!empty($aData)) {
		         $oTables = $this->fillVo($aData, $oTables);
		     }
		}
		return $oTables;
	}


	/**
	 * This method is ment to be called by save so any pre and post events are triggered also.
	 * Store form data, please first perform validation by calling validate
	 * @param array $aData an array of fields that belong to this type of data
	 * @return Tables
	 * @throws \Propel\Runtime\Exception\PropelException
	 */
	public function store(array $aData = null): Tables
	{
		$oTables = $this->getModel($aData);


		 if(!empty($oTables))
		 {
		     $oTables = $this->fillVo($aData, $oTables);
		     $oTables->save();
		 }
		return $oTables;
	}


	/**
	 * Fills the model object with data comming from a client.
	 * @param array $aData
	 * @param Tables $oModel
	 * @return Tables
	 */
	protected function fillVo(array $aData, Tables $oModel): Tables
	{
		isset($aData['name']) ? $oModel->setName($aData['name']) : null;
		isset($aData['icon']) ? $oModel->setIcon($aData['icon']) : null;
		isset($aData['title']) ? $oModel->setTitle($aData['title']) : null;
		isset($aData['form']) ? $oModel->setForm($aData['form']) : null;
		isset($aData['type']) ? $oModel->setType($aData['type']) : null;
		isset($aData['required']) ? $oModel->setRequired($aData['required']) : null;
		return $oModel;
	}
}
