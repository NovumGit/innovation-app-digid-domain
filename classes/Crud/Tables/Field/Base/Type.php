<?php
namespace Crud\Custom\NovumDigid\Tables\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'type' crud field from the 'tables' table.
 * This class is auto generated and should not be modified.
 */
abstract class Type extends GenericString implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'type';

	protected $sFieldLabel = 'Titel';

	protected $sIcon = 'list';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getType';

	protected $sFqModelClassname = '\Model\Custom\NovumDigid\Tables';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['type']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Titel" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
