<?php 
namespace Crud\Custom\NovumDigid\Tables\Field\Base;

use Crud\Generic\Field\GenericDelete;
use Crud\IEventField;
use Model\Custom\NovumDigid\Tables;

abstract class Delete extends GenericDelete implements IEventField
{
	public function getDeleteUrl($oObject = null)
	{
		if($oObject instanceof Tables)
		{
		     return "/custom/novumdigid/systeem/tables/overview?_do=ConfirmDelete&id=" . $oObject->getId();
		}
		return '';
	}


	public function getIcon(): string
	{
		return "trash";
	}


	public function getUnDeleteUrl($oObject = null)
	{
		if($oObject instanceof Tables)
		{
		     return "/custom/novumdigid/tables?_do=UnDelete&id=" . $oObject->getId();
		}
		return '';
	}
}
