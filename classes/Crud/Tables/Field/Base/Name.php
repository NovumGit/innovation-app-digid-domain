<?php
namespace Crud\Custom\NovumDigid\Tables\Field\Base;

use Crud\Generic\Field\GenericString;
use Crud\IEditableField;
use Crud\IFilterableField;
use Crud\IRequiredField;

/**
 * Base class that represents the 'name' crud field from the 'tables' table.
 * This class is auto generated and should not be modified.
 */
abstract class Name extends GenericString implements IFilterableField, IEditableField, IRequiredField
{
	protected $sFieldName = 'name';

	protected $sFieldLabel = 'Name';

	protected $sIcon = 'tag';

	protected $sPlaceHolder = '';

	protected $sGetter = 'getName';

	protected $sFqModelClassname = '\Model\Custom\NovumDigid\Tables';


	public function isUniqueKey(): bool
	{
		return false;
	}


	public function hasValidations()
	{
		return true;
	}


	public function validate($aPostedData)
	{
		$mResponse = false;
		$mParentResponse = parent::validate($aPostedData);


		if(!isset($aPostedData['name']))
		{
		     $mResponse = [];
		     $mResponse[] = 'Het veld "Name" verplicht maar nog niet ingevuld.';
		}
		if(!empty($mParentResponse)){
		     $mResponse = array_merge($mResponse, $mParentResponse);
		}
		return $mResponse;
	}
}
