<?php
namespace Crud\Custom\NovumDigid\Tables\Field;

use Crud\Custom\NovumDigid\Tables\Field\Base\Required as BaseRequired;

/**
 * Skeleton subclass for representing required field from the tables table .
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
final class Required extends BaseRequired
{
}
